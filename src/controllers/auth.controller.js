import User from "../models/User";

export const getUser = async (req, res) => {
  try {
    const user = await User.find({
      country: req.body.country,
      code: req.body.code,
    });
    res.status(200).json(user);
  } catch (e) {
    throw new Error(e.message);
  }
};
