import Room from "../models/Room";

export const createRoom = async (req, res) => {
  try {
    const { place, title, description, stars, reviews, img, like } = req.body;

    const newRoom = new Room({
      place,
      title,
      description,
      stars,
      reviews,
      img,
      like,
    });

    const roomSave = await newRoom.save();
    res.status(201).json(roomSave);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const getRooms = async (req, res) => {
  try {
    const rooms = await Room.find();
    res.status(200).json(rooms);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const searchRoom = async (req, res) => {
  try {
    const rooms = await Room.find({
      $or: [
        { title: { $regex: req.params.searchValue } },
        { place: { $regex: req.params.searchValue } },
        { description: { $regex: req.params.searchValue } },
        { reviews: { $regex: req.params.searchValue } },
        { stars: { $regex: req.params.searchValue } },
        { activity: { $regex: req.params.searchValue } },
        { flexibility: { $regex: req.params.searchValue } },
      ],
    });
    res.status(200).json(rooms);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const filterByDescription = async (req, res) => {
  try {
    let rooms;
    if (req.params.filter === "description") {
      rooms = await Room.find({
        [req.params.filter]: { $regex: req.params.value },
      });
    } else {
      rooms = await Room.find({
        [req.params.filter]: req.params.value,
      });
    }
    res.status(200).json(rooms);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const getRoomByHour = async (req, res) => {
  try {
    const room = await Room.find({
      $and: [{ activity: req.body.activity }, { arrival: req.body.hour }],
    });

    res.status(201).json(room);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const getRoomByTime = async (req, res) => {
  try {
    const room = await Room.find({
      $or: [
        { activity: req.body.activity },
        { arrival: req.body.arrival },
        { exit: req.body.exit },
        { participants: req.body.participants },
      ],
    });

    res.status(201).json(room);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const updateLike = async (req, res) => {
  try {
    const { like } = req.body;
    const room = await Room.findById(req.params.id);
    room.like = like;
    room.save();

    res.status(201).json(room);
  } catch (e) {
    throw new Error(e.message);
  }
};
