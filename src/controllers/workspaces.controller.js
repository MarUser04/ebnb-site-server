import Workspace from "../models/Workspace";

export const getWorkspaces = async (req, res) => {
  try {
    const workspaces = await Workspace.find();
    res.status(200).json(workspaces);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const createWorkspace = async (req, res) => {
  try {
    const { title, text } = req.body;

    const newWorkspace = new Workspace({
      title,
      text,
    });

    const workspaceSave = await newWorkspace.save();
    res.status(201).json(workspaceSave);
  } catch (e) {
    throw new Error(e.message);
  }
};
