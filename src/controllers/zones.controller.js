import Zone from "../models/Zone";

export const getZones = async (req, res) => {
  try {
    const zones = await Zone.find();
    res.status(200).json(zones);
  } catch (e) {
    throw new Error(e.message);
  }
};

export const createZone = async (req, res) => {
  try {
    const { title, ubication } = req.body;

    const newZone = new Zone({
      title,
      ubication,
    });

    const zoneSave = await newZone.save();
    res.status(201).json(zoneSave);
  } catch (e) {
    throw new Error(e.message);
  }
};
