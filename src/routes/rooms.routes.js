import { Router } from "express";
import {
  getRooms,
  createRoom,
  searchRoom,
  updateLike,
  filterByDescription,
  getRoomByHour,
  getRoomByTime,
} from "../controllers/rooms.controller";

const router = Router();

router.get("/", getRooms);
router.get("/:filter/:value", filterByDescription);
router.get("/:searchValue", searchRoom);
router.post("/", createRoom);
router.post("/hour", getRoomByHour);
router.post("/time", getRoomByTime);
router.put("/:id", updateLike);

export default router;
