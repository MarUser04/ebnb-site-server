import { Router } from "express";
import {
  getWorkspaces,
  createWorkspace,
} from "../controllers/workspaces.controller";

const router = Router();

router.get("/", getWorkspaces);
router.post("/", createWorkspace);

export default router;
