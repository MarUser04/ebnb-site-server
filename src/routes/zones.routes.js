import { Router } from "express";
import { getZones, createZone } from "../controllers/zones.controller";

const router = Router();

router.get("/", getZones);
router.post("/", createZone);

export default router;
