import { Router } from "express";
import { getUser } from "../controllers/auth.controller";

const router = Router();

router.post("/", getUser);

export default router;
