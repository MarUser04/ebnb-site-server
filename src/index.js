import express from "express";
import regeneratorRuntime from "regenerator-runtime";
import cors from "cors";
import "dotenv/config";

//Databases connection
import "./database";

//Routes
import authRoutes from "./routes/auth.routes";
import zonesRoutes from "./routes/zones.routes";
import roomsRoutes from "./routes/rooms.routes";
import workspacesRoutes from "./routes/workspaces.routes";

const app = express();

app.use(express.json());
app.use(cors());

//Routes
app.use("/api/auth", authRoutes);
app.use("/api/zones", zonesRoutes);
app.use("/api/rooms", roomsRoutes);
app.use("/api/workspaces", workspacesRoutes);

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Listening on port ${port}`));
