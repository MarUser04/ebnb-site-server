import { Schema, model } from "mongoose";

const workSpaceSchema = new Schema(
  {
    title: String,
    text: String,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Workspace", workSpaceSchema);
