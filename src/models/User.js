import { Schema, model } from "mongoose";

const userSchema = new Schema(
  {
    country: String,
    code: String,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("User", userSchema);
