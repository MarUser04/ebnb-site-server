import { Schema, model } from "mongoose";

const roomSchema = new Schema(
  {
    place: String,
    title: String,
    description: String,
    stars: String,
    reviews: String,
    img: String,
    like: Boolean,
    activity: String,
    flexibility: String,
    arrival: String,
    exit: String,
    participants: Number,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Room", roomSchema);
