import { Schema, model } from "mongoose";

const zonesSchema = new Schema(
  {
    title: String,
    ubication: String,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

export default model("Zone", zonesSchema);
